# Deploy django by Nginx and Gunicorn

> author : 林冠曄
>
> last update : 2019/11/27

## Brief

這篇主要在記錄如何用ubuntu + nginx + gunicorn 部署。

## Enviornment

`Ubuntu 18.04` : 伺服器

`nginx ` ：處理代理

`gunicorn` ：處理Django

`python 3.6.3`

`django 2.2.6`

`虛擬環境` : 避免ubuntu 預設python 2.7的麻煩 （以及其他可能的麻煩）

## Note

1. 假設專案在家目錄 ~/GoStar
2. 若需要root 請 sudo -s
3. 建立完虛擬環境後，請在虛擬環境執行

## Steps-python & Django

### 更新系統

in terminal:

```bash
sudo apt-get update
sudo apt-get dis-upgrade
```

### 安裝python 3.6.3

in terminal:

```bash
apt-get install python-dev
apt-get install python-pip
pip install --upgrade pip
```

### 在家目錄弄pip的configure

.pip 是隱藏檔案 若要看要 ls -al

in terminal:

```bash
mkdir ~/.pip
nano ~/.pip/pip.conf
```



```sh
# in pip.conf
[global]
index-url = http://mirrors.aliyun.com/pypi/simple/

[install]
trusted-host=mirrors.aliyun.com

[list]
format=columns
```

### 建立虛擬環境： 在家目錄，建立名為django的虛擬環境

in terminal:

```bash
python3.6 -m venv ~/django
```

> 可能出現錯誤說沒有安裝虛擬環境，照著安裝即可

### 啟動虛擬環境

in terminal:

```bash
source ~/django/bin/activate
```

> 此時會看到終端機最前面出現 (django)

### 建立專案所需要的package清單

in terminal:

```bash
cd ~/GoStar
pip freeze > requirements.txt
```

### 安裝環境包

in terminal:

```bash
pip install -r requirements.txt
```

### 用django內建server來測試

> 預設為127.0.0.1:8000
> 可以改為 0.0.0.0:80 就可以在網路上看到
>
> > port 80 需要root

```bash
python manage.py runserver
```

> 進入127.0.0.1:8000 看有沒有成功
> 若沒有圖形介面可以這樣：
>
> ```bash
> curl 127.0.0.1:8000
> ```
>
> 此會把html以文字顯示

> crtl + c 可以關閉
>
> 若不小心沒有關掉可以手動
>
> ```bash
> fuser -k 80/tcp
> ```
>
> -> 殺掉tcp在80的服務
>
> 確認服務狀態
>
> ```bash
> netstat -anp
> ```

以上過程可以完成簡單的部署 （測試版）。

但因為django 的server 負載小，所以要用別的東西掛，

以下用`gunicorn`來處理非靜態，`nginx`處理靜態及請求、(反向)代理

## Steps - gunicorn

```bash
pip install gunicorn
```

### 用專案測試

```bash
cd ~/GoStar
gunicorn GoStar.wsgi:application
```

> 以gunicorn 掛 GoStar裡面 wsgi.py檔案
>
> 預設以127.0.0.1:8000來跑

### 加入參數可以用別的port and ip

```bash
gunicorn -b 0.0.0.0:80 GoStar.wsgi:application
```

> -> 用80要root
>
> -> 在背景執行 後面加 &

### log level

```bash
gunicorn --log-level error
```

> 等級有：debug, info, warning, error, critical 可用

### note: 

- 之後會在127.0.0.1:8000執行就可以
- 會用nginx 代理到這邊

---

## Steps - nginx

以下開始處理 nginx，記得在虛擬環境下執行，可能需要`root`

### 安裝

```bash
apt-get install nginx
```

### 處理configure

```bash
nano /etc/nginx/sites-enabled/nginx.conf
```

```
# /etc/nginx/sites-enabled/nginx.conf
server {
    # 80 port
    listen      80;
    # domain name
    server_name gostar4f-1.nknu.edu.tw;
    charset     utf-8;

    #Max upload size
    client_max_body_size 75M;   # adjust to taste
    
    # Django media
    # location /media  {
    #    alias /var/www/path/to/your/project/media;      # your Django project's media files
    # }

    # location /assets {
    #     alias /var/www/path/to/your/project/static;     # your Django project's static files
    # }

    # Finally, send all non-media requests to the Django server.
    location / {
        proxy_pass http://127.0.0.1:8000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
```

> [6] domain name
>
> [13] 定義media路徑
>
> [17] 定義static路徑
>
> [23] 定義django用gunicorn運行時的位址

`crtl + x, y, enter`退出並儲存

### 移除default nginx

```bash
rm /etc/nginx/sites-enabled/default
```

### 重啟nginx

```bash
sudo systemctl restart nginx
```

### 啟動gunicorn

```bash
gunicorn ~/GoStar.GoStar.wsgi:application
```

## Reference

https://www.jianshu.com/p/d6f9138fab7b
https://gist.github.com/Atem18/4696071
https://www.digitalocean.com/community/tutorials/how-to-deploy-python-wsgi-apps-using-gunicorn-http-server-behind-nginx
https://www.zmrenwu.com/courses/django-blog-tutorial/materials/15/
https://www.jianshu.com/p/5600af9ff238

