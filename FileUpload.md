# File Upload Problems

> author : 林冠曄
>
> last update : 2019/11/26

## Brief

1. Define media root and models in your apps with file field or image field
2. Use django.forms to automatically generate the form
3. write your view and html

*note that you can add file without defining models, which is not recommended since the management may be hard*

---

## About

​	在專案中的 `/user` 欲加入上傳頭貼功能。（不用這個功能的話，只能在後台新增。）

## Steps

### Step 1. Define media root

到設定裡面設定`media root` 跟` url `目的是讓 django 知道：

1. 上傳到哪邊
2. 網址要叫什麼

想當然爾，url跟root 可以不用長得一樣。

在專題中因為考慮到之後系統上線會有多位使用者，如果我把路徑設定在gitlab的資料夾裡面，

未來在pull的時候會有一堆檔案。另一個則是安全性考量。

因此最後設定在伺服器某個資料夾上。但為了開發方便，所以設定「當開發者使用的時候，用專案裡的資料夾」

```python
# setting.py
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
# media root 可以設定某個絕對位址
MEDIA_URL = '/media/'
# url設定為 xxx/media/

```

### Step 2. Define url.py

```python
# GoStar.url.py
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from GoStar import settings

# ... your url ... #
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```

### Step 3. Define models.py

```python
# user.models.py
from django.db import models
import os, uuid

def user_directory_path(instance, filename):
	ext = filename.split('.')[-1]
	filename = '{}.{}'.format(uuid.uuid4().hex, ext)
	return os.path.join('avatar', filename)

class UserInfo(models.Model):
    # ... other ... #
    imageAvatar = models.ImageField(upload_to = user_directory_path, default = '', blank = True)
```

> [4] 定義上傳位置跟檔案名稱
>
> [5] 取出副檔名
>
> [6] 用`uuid`重新命名 , `hex` 讓uuid的 `-` 消失
>
> [11] `upload_to` 定義上傳位址，可以直接定義一個地點，例如`'/avatar' `就會上傳到 `'/media/avatar'`

### Step 4. Define form.py

```python
from django import forms
from user.models import *

# ... some other forms ... #
class UploadImageForm(forms.ModelForm):
    class Meta:
        model = UserInfo
        fields = ['imageAvatar']
```

> [6] 讓他自己根據models來定義form的欄位
>
> [8] 這裡只有定義一個欄位，**但是**實際在這個`class`還有其他欄位，所以等等`view`不能用 `form.save()`
> 	 因為`form.save() `會儲存那個`class`的所有資料，不管`form`有沒有定義，因此會變成其他都`null`

### Step 5. Write view.py

```python
@login_required
def user(request):
# ... something else ... #
    form = UploadImageForm()
	if request.method == "POST":
		form = UploadImageForm(request.POST, request.FILES)
		if form.is_valid():
			userinfo.imageAvatar = form.cleaned_data['imageAvatar']
			userinfo.save()
			return redirect("/user/")
	else:
		form = UploadImageForm()
 # ... something else ... #
```

> [6] 記得要 `request.POST` 跟 `request.FILES`都要
>
> [7] 如果驗證通過（因為`ImageField`ˋ自帶驗證，所以就不另外寫）
>
> [8] `userinfo.imageAvatar`欄位拿 `cleaned_data`對應欄位
>
> [9] 儲存`userinfo`
>
> **Note** 這邊如果用 `form.save()`會出現錯誤，因為form沒有定義其他該表的欄位，會變成空值。 

### Step 6. add form to html template

```html
<form action="/user/" method="post" enctype="multipart/form-data" >
    {% csrf_token %}
    {{ form.imageAvatar }}
    <button type="submit" value="submit">Upload</button>
</form>
```

> [1] 注意 `enctype`一定要寫這樣才能拿到檔案

---

然後就搞定囉！

### 過程中犯的蠢

- 因為之前把``userinfo`` 變成一個``dictionary``，導致在``userinfo.save()`` 的時候出問題

 